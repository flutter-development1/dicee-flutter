import 'dart:math';

import 'package:flutter/material.dart';

void main() {
  return runApp(
    MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.red,
        appBar: AppBar(
          title: Text('Dicee'),
          backgroundColor: Colors.red,
        ),
        body: DicePage(),
      ),
    ),
  );
}

class DicePage extends StatefulWidget {
  @override
  _DicePageState createState() => _DicePageState();
}

class _DicePageState extends State<DicePage> {
  int leftDiceNbr = 1;
  int rightDiceNbr = 2;
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Expanded(
            flex: 1,
            child: FlatButton(
              child: Image.asset('images/dice$leftDiceNbr.png'),
              onPressed: () {
                setState(() {
                  randomizeDice();
                });
              },
            ),
          ),
          Expanded(
            flex: 1,
            child: FlatButton(
              child: Image.asset('images/dice$rightDiceNbr.png'),
              onPressed: () {
                setState(() {
                  randomizeDice();
                });
              },
            ),
          )
        ],
      ),
    );
  }

  void randomizeDice() {
    leftDiceNbr = new Random().nextInt(6) + 1;
    rightDiceNbr = new Random().nextInt(6) + 1;
  }
}
